﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace one_wallpaper_engine
{
    public partial class MainForm : Form
    {
        private WallpaperEngineForm wpeForm;
        private Bitmap[] previewArr = new Bitmap[4];

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            previewArr[0] = global::one_wallpaper_engine.Properties.Resources.pv0;
            previewArr[1] = global::one_wallpaper_engine.Properties.Resources.pv1;
            previewArr[2] = global::one_wallpaper_engine.Properties.Resources.pv2;
            previewArr[3] = global::one_wallpaper_engine.Properties.Resources.pv3;
            this.WallList.DataSource = new String[] { "你的名字", "江南少女（有声版）", "Redial", "极乐净土" };
        }

        /// <summary>
        /// 启用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnStart_Click(object sender, EventArgs e)
        {
            String url = System.AppDomain.CurrentDomain.BaseDirectory + "content\\" + this.WallList.SelectedIndex + ".mp4";
            if (wpeForm == null)
            {
                this.BtnRecovery.Enabled = true;
                this.CboPause.Enabled = true;
                wpeForm = new WallpaperEngineForm();
                wpeForm.url = url;
                wpeForm.isMute = this.CboMute.Checked;
                wpeForm.Show();
            } else
            {
                wpeForm.setWMPUrl(url);
            }
        }

        /// <summary>
        /// 恢复
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnRecovery_Click(object sender, EventArgs e)
        {
            if (wpeForm != null)
            {
                this.BtnRecovery.Enabled = false;
                this.CboMute.Checked = false;
                this.CboPause.Checked = false;
                this.CboPause.Enabled = false;
                wpeForm.recovery();
                wpeForm = null;
            }
        }

        /// <summary>
        /// 窗口关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (wpeForm != null)
            {
                wpeForm.recovery();
                wpeForm = null;
            }
        }

        /// <summary>
        /// 是否静音
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboMute_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox item = (CheckBox) sender;
            if (wpeForm != null)
            {
                wpeForm.setWMPMute(item.Checked);
            }
        }

        /// <summary>
        /// 是否暂停
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboPause_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox item = (CheckBox)sender;
            if (wpeForm != null)
            {
                wpeForm.setWMPPlayOrPause(item.Checked);
            }
        }

        private void CboContent_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox item = (ComboBox)sender;
            this.PicPreview.Image = previewArr[item.SelectedIndex];
        }

        private void WallList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox item = (ListBox)sender;
            this.PicPreview.Image = previewArr[item.SelectedIndex];
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            if (wpeForm != null)
            {
                wpeForm.recovery();
                wpeForm = null;
            }
            Application.Exit();
        }

        private void WallList_DoubleClick(object sender, EventArgs e)
        {
            String url = System.AppDomain.CurrentDomain.BaseDirectory + "content\\" + this.WallList.SelectedIndex + ".mp4";
            if (wpeForm == null)
            {
                this.BtnRecovery.Enabled = true;
                this.CboPause.Enabled = true;
                wpeForm = new WallpaperEngineForm();
                wpeForm.url = url;
                wpeForm.isMute = this.CboMute.Checked;
                wpeForm.Show();
            }
            else
            {
                wpeForm.setWMPUrl(url);
            }
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("描述：C#实现类似WallpaperEngin的应用程序\r\n作者：Westin Yang\r\n邮箱：517858177@qq.com", "关于");
        }
    }
}
