﻿namespace one_wallpaper_engine
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.BtnStart = new System.Windows.Forms.Button();
            this.BtnRecovery = new System.Windows.Forms.Button();
            this.CboMute = new System.Windows.Forms.CheckBox();
            this.CboPause = new System.Windows.Forms.CheckBox();
            this.PicPreview = new System.Windows.Forms.PictureBox();
            this.WallList = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.开始ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.PicPreview)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnStart
            // 
            this.BtnStart.Location = new System.Drawing.Point(10, 237);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(120, 40);
            this.BtnStart.TabIndex = 0;
            this.BtnStart.Text = "启用";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // BtnRecovery
            // 
            this.BtnRecovery.Enabled = false;
            this.BtnRecovery.Location = new System.Drawing.Point(136, 237);
            this.BtnRecovery.Name = "BtnRecovery";
            this.BtnRecovery.Size = new System.Drawing.Size(196, 40);
            this.BtnRecovery.TabIndex = 1;
            this.BtnRecovery.Text = "恢复";
            this.BtnRecovery.UseVisualStyleBackColor = true;
            this.BtnRecovery.Click += new System.EventHandler(this.BtnRecovery_Click);
            // 
            // CboMute
            // 
            this.CboMute.AutoSize = true;
            this.CboMute.Location = new System.Drawing.Point(13, 293);
            this.CboMute.Name = "CboMute";
            this.CboMute.Size = new System.Drawing.Size(48, 16);
            this.CboMute.TabIndex = 0;
            this.CboMute.Text = "静音";
            this.CboMute.UseVisualStyleBackColor = true;
            this.CboMute.CheckedChanged += new System.EventHandler(this.CboMute_CheckedChanged);
            // 
            // CboPause
            // 
            this.CboPause.AutoSize = true;
            this.CboPause.Enabled = false;
            this.CboPause.Location = new System.Drawing.Point(82, 293);
            this.CboPause.Name = "CboPause";
            this.CboPause.Size = new System.Drawing.Size(48, 16);
            this.CboPause.TabIndex = 1;
            this.CboPause.Text = "暂停";
            this.CboPause.UseVisualStyleBackColor = true;
            this.CboPause.CheckedChanged += new System.EventHandler(this.CboPause_CheckedChanged);
            // 
            // PicPreview
            // 
            this.PicPreview.Image = global::one_wallpaper_engine.Properties.Resources.pv0;
            this.PicPreview.Location = new System.Drawing.Point(136, 35);
            this.PicPreview.Name = "PicPreview";
            this.PicPreview.Size = new System.Drawing.Size(196, 196);
            this.PicPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicPreview.TabIndex = 2;
            this.PicPreview.TabStop = false;
            // 
            // WallList
            // 
            this.WallList.FormattingEnabled = true;
            this.WallList.ItemHeight = 12;
            this.WallList.Location = new System.Drawing.Point(10, 35);
            this.WallList.Name = "WallList";
            this.WallList.Size = new System.Drawing.Size(120, 196);
            this.WallList.TabIndex = 3;
            this.WallList.SelectedIndexChanged += new System.EventHandler(this.WallList_SelectedIndexChanged);
            this.WallList.DoubleClick += new System.EventHandler(this.WallList_DoubleClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Gainsboro;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.开始ToolStripMenuItem,
            this.AboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(344, 25);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 开始ToolStripMenuItem
            // 
            this.开始ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExitToolStripMenuItem});
            this.开始ToolStripMenuItem.Name = "开始ToolStripMenuItem";
            this.开始ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.开始ToolStripMenuItem.Text = "菜单";
            // 
            // AboutToolStripMenuItem
            // 
            this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
            this.AboutToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.AboutToolStripMenuItem.Text = "关于";
            this.AboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ExitToolStripMenuItem.Text = "退出";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 321);
            this.Controls.Add(this.WallList);
            this.Controls.Add(this.CboPause);
            this.Controls.Add(this.PicPreview);
            this.Controls.Add(this.CboMute);
            this.Controls.Add(this.BtnRecovery);
            this.Controls.Add(this.BtnStart);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "壁纸引擎 V1.0";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PicPreview)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnStart;
        private System.Windows.Forms.Button BtnRecovery;
        private System.Windows.Forms.CheckBox CboMute;
        private System.Windows.Forms.CheckBox CboPause;
        private System.Windows.Forms.PictureBox PicPreview;
        private System.Windows.Forms.ListBox WallList;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 开始ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
    }
}