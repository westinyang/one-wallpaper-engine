﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace one_wallpaper_engine
{
    /// <summary>
    /// 主窗口
    /// </summary>
    public partial class WallpaperEngineForm : Form
    {
        private int SCREEN_WIDTH = Screen.PrimaryScreen.Bounds.Width;
        private int SCREEN_HEIGHT = Screen.PrimaryScreen.Bounds.Height;
        private IntPtr workerw;

        public String url = "";
        public bool isMute = false; // 是否静音
        public bool isPause = false; // 是否暂停

        public WallpaperEngineForm()
        {
            InitializeComponent();
        }

        private void WallpaperEngine_Load(object sender, EventArgs e)
        {
            this.ClientSize = new System.Drawing.Size(SCREEN_WIDTH, SCREEN_HEIGHT);
            this.WMP.URL = url;
            this.WMP.settings.setMode("loop", true);    //设置循环播放
            this.WMP.settings.mute = isMute;
            initWPE();
        }

        /// <summary>
        /// 初始化壁纸引擎
        /// </summary>
        public void initWPE ()
        {
            // 查找Progman窗口句柄
            IntPtr progman = W32.FindWindow("Progman", null);

            IntPtr result = IntPtr.Zero;

            // 发送消息（0x052C）给Progman窗口
            // 此消息指示Progman窗口在桌面图标窗口下面创建WorkerW窗口
            // 如果已经创建则没有任何变化
            W32.SendMessageTimeout(progman, 0x052C, new IntPtr(0), IntPtr.Zero, W32.SendMessageTimeoutFlags.SMTO_NORMAL, 1000, out result);

            workerw = IntPtr.Zero;

            // 枚举所有的窗口，直到找到子窗口包含一个SHELLDLL_DefView的
            // 如果发现该窗口，我们直接把该窗口的句柄赋值给workerw变量
            W32.EnumWindows(new W32.EnumWindowsProc((tophandle, topparamhandle) =>
            {
                IntPtr p = W32.FindWindowEx(tophandle, IntPtr.Zero, "SHELLDLL_DefView", IntPtr.Zero);
                if (p != IntPtr.Zero)
                {
                    // 获取当前的WorkerW窗口
                    workerw = W32.FindWindowEx(IntPtr.Zero, tophandle, "WorkerW", IntPtr.Zero);
                }
                return true;
            }), IntPtr.Zero);

            // 设置当前窗口父窗句柄为workerw
            W32.SetParent(this.Handle, workerw);
        }

        /// <summary>
        /// 恢复壁纸
        /// </summary>
        public void recovery()
        {
            W32.SetWindowPos(workerw, IntPtr.Zero, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, W32.SetWindowPosFlags.HideWindow);
            this.Dispose();
            W32.SetWindowPos(workerw, IntPtr.Zero, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, W32.SetWindowPosFlags.ShowWindow);
        }

        /// <summary>
        /// 设置是否静音
        /// </summary>
        /// <param name="isMute"></param>
        public void setWMPMute(bool isMute)
        {
            this.WMP.settings.mute = isMute;
        }

        public void setWMPPlayOrPause(bool isPause)
        {
            if(isPause)
            {
                this.WMP.Ctlcontrols.pause();
            } else
            {
                this.WMP.Ctlcontrols.play();
            }
        }

        public void setWMPUrl(String url)
        {
            this.WMP.URL = url;
        }

        private void WallpaperEngineForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void WallpaperEngine_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
